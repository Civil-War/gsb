<?php 
    session_start();
    include("modeles/class.PdoGSB.inc.php");

    if(!isset($_REQUEST['uc']))
    {
    	$uc = 'acces';
    }
    else
    {
    	$uc = $_REQUEST['uc'];
    }

    
    $pdo = PdoGSB::getPdoGSB();

    switch($uc)
    {
        case 'acces':
        {
        	include('vues/v_entete.php');
	        include("controleurs/c_connexion.php");
	        break; 	
        }

        case 'usager':
        {
        	if(isset($_SESSION['login']))
        	{
        		include ('vues/Usager/v_entete.php');
        		include ('vues/Usager/v_menu.php');
        		include ('controleurs/c_gestionUsager.php');
        		include ('vues/Usager/v_footer.php');
        		break;
        	}
        	else
        	{
        		echo "<script>document.location.replace('index.php?uc=acces&action=seConnecterEnUsager');</script>";
        	}
        }

        case 'administrer':
        {
        	if(isset($_SESSION['email']))
        	{
        		include ('vues/Admin/v_entete.php');
        		include ('vues/Admin/v_menu.php');
        		include ('controleurs/c_gestionAdmin.php');
        		include ('vues/Admin/v_footer.php');
        		break;
        	}
        	else
        	{
        		echo "<script>document.location.replace('index.php?uc=acces&action=seConnecterEnAdmin');</script>";
        	}
        }




    }

?>