
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="index.php?uc=administrer&action=accueilAdmin" class="logo">
                Administration
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->
                        <li class="dropdown messages-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-envelope"></i>
                                <span class="label label-success"><?php echo $_SESSION['nbMessage']; ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">Votre Messagerie</li>
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu">
                                        <?php
                                        $LesMessages = $pdo->chargerLesMessages($_SESSION['email']);

                                        foreach($LesMessages AS $UnMessage)
                                        {
                                            $id = $UnMessage['id'];
                                            $adresse = $UnMessage['adresseExpediteur'];
                                            $sujet = $UnMessage['sujet'];

                                            echo "<li>
                                                <a href='index.php?uc=administrer&action=voirMessage&id=$id'>
                                                    <div class='pull-left'>
                                                        <i class='fa fa-commenting' aria-hidden='true'></i>
                                                    </div>
                                                    <h4>
                                                        $adresse
                                                    </h4>
                                                    <p>$sujet</p>
                                                    <small class='pull-right'><i class='fa fa-clock-o'></i></small>
                                                </a>
                                            </li>";
                                        }
                                        ?>
                                    </ul>
                                </li>
                                <li class="footer"><a href="#">See All Messages</a></li>
                            </ul>
                        </li>
                        
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-user"></i>
                                <span><?php echo $_SESSION['nomAdmin']." ".$_SESSION['prenomAdmin']; ?> <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
                                <li class="dropdown-header text-center">Compte</li>

                                <li>
                                    <a href="index.php?uc=administrer&action=envoyerMessage">
                                    <i class="fa fa-envelope-o fa-fw pull-right"></i>
                                     Envoyer un message</a>
                                </li>

                                <li class="divider"></li>

                                    <li>
                                        <a href="#">
                                        <i class="fa fa-user fa-fw pull-right"></i>
                                            Profils
                                        </a>
                                        <a data-toggle="modal" href="#modal-user-settings">
                                        <i class="fa fa-cog fa-fw pull-right"></i>
                                            Options
                                        </a>
                                        </li>
                                        <li class="divider"></li>
                                        <li>
                                            <a href="index.php?uc=administrer&action=seDeconnecter"><i class="fa fa-ban fa-fw pull-right"></i> Deconnexion</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </header>
                <div class="wrapper row-offcanvas row-offcanvas-left">
                    <!-- Left side column. contains the logo and sidebar -->
                    <aside class="left-side sidebar-offcanvas">
                        <!-- sidebar: style can be found in sidebar.less -->
                        <section class="sidebar">
                            <!-- Sidebar user panel -->
                            <div class="user-panel">
                                <div class="pull-left image">
                                    <img src="img/26115.jpg" class="img-circle" alt="User Image" />
                                </div>
                                <div class="pull-left info">
                                    <p>Hello, <?php echo $_SESSION['prenomAdmin']; ?></p>

                                    <i class="fa fa-circle text-success"></i> Online
                                </div>
                            </div>
                            <!-- search form -->
                            <form action="#" method="get" class="sidebar-form">
                                <div class="input-group">
                                    <input type="text" name="q" class="form-control" placeholder="Search..."/>
                                    <span class="input-group-btn">
                                        <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                            </form>
                            <!-- /.search form -->
                            <!-- sidebar menu: : style can be found in sidebar.less -->
                            <ul class="sidebar-menu">
                                <li class="active">
                                    <a href="index.php?uc=administrer&action=accueilAdmin">
                                        <i class="fa fa-dashboard"></i> <span>Tableau de bord</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="index.php?uc=administrer&action=consulterEvenements&q=voirEvenements">
                                        <i class="fa fa-calendar"></i> <span>Evènements</span>
                                    </a>
                                </li>
								<li>
                                    <a href="index.php?uc=administrer&action=consulterEmployers&q=voirEmployers">
                                        <i class="fa fa-users"></i> <span>Employés</span>
                                    </a>
                                </li>
                            </ul>
                        </section>
                        <!-- /.sidebar -->
                    </aside>