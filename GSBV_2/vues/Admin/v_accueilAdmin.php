         <aside class="right-side">

                <!-- Main content -->
                <section class="content">


                    <div class="row">

                        <div class="col-md-8">
                            <section class="panel">
                              <header class="panel-heading">
                                  Liste des évenements organisés
                            </header>
                            <div class="panel-body table-responsive">
                                <table class="table table-hover">
                                  <thead>
                                    <tr>
                                      <th>Lieu</th>
                                      <th>Date de l'évènement</th>
                                      <th>Heure de l'évènement</th>
                                      <th>Description</th>
                                      <th>Employés</th>
                                      <th>Modifier</th>
                                      <th>Supprimer</th>
                                  	</tr>
                              </thead>
                              <tbody>
                              <?php 
                              	foreach($LesEvenements as $UnEvenement)
                              	{
                              		$id = $UnEvenement['idE'];
                              		$lieu = $UnEvenement['lieu'];
                              		$dateE = $UnEvenement['dateE'];
                              		$heureE = $UnEvenement['heureE'];
                              		$Description = $UnEvenement['Description'];
                              		$idEm = $UnEvenement['idAdmin'];

                              		echo "<tr>
			                                  <td>$lieu</td>
			                                  <td>$dateE</td>
			                                  <td>$heureE</td>
			                                  <td>$Description</td>
			                                  <td>$idEm</td>
			                                  <td><a href='index.php?uc=administrer&action=modifierEvenements&id=$id'>Modifier : $Description </a></td>
			                                  <td><a href='index.php?uc=administrer&action=supprimerEvenements&id=$id'>Supprimer : $Description </a></td>
			                              </tr>";
                              	}
                               ?>
                          </tbody>
                      </table>
                  </div>
                  </div>
                  </div>

                   <div class="row">

                        <div class="col-md-8">
                            <section class="panel">
                              <header class="panel-heading">
                                  Liste des employés
                            </header>
                            <div class="panel-body table-responsive">
                                <table class="table table-hover">
                                  <thead>
                                    <tr>
                                      <th>Nom</th>
                                      <th>Prenom</th>
                                      <th>Login</th>
                                      <th>Mot de passe</th>
                                      <th>Id du Service</th>
                                  	</tr>
                              </thead>
                              <tbody>
                              <?php 
                              	foreach($LesEmployes as $UnEmploye)
                              	{
                              		$id = $UnEmploye['idEm'];
                              		$nom = $UnEmploye['nom'];
                              		$prenom = $UnEmploye['prenom'];
                              		$login = $UnEmploye['login'];
                              		$pass = $UnEmploye['mdp'];
                              		$idS = $UnEmploye['idS'];

                              		echo "<tr>
			                                  <td>$nom</td>
			                                  <td>$prenom</td>
			                                  <td>$login</td>
			                                  <td>$pass</td>
			                                  <td>$idS</td>
			                                  <td><a href='index.php?uc=administrer&action=modifierEmployes&id=$id'>Modifier l'employé $nom </a></td>
			                                  <td><a href='index.php?uc=administrer&action=supprimerEmployes&id=$id'>Supprimer l'employé $nom </a></td>
			                              </tr>";
                              	}
                               ?>
                          </tbody>
                      </table>
                  </div>
              </section>


         
            </aside><!-- /.right-side -->

