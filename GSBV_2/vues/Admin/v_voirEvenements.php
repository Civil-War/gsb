          <aside class="right-side">

                <!-- Main content -->
                <section class="content">


                    <div class="row">

                        <div class="col-md-8">
                            <section class="panel">
                              <header class="panel-heading">
                                  Liste des évenements organisés
                            </header>
                            <div class="panel-body table-responsive">
                                <table class="table table-hover">
                                  <thead>
                                    <tr>
                                      <th>Lieu</th>
                                      <th>Date de l'évènement</th>
                                      <th>Heure de l'évènement</th>
                                      <th>Description</th>
                                      <th>Organisé par</th>
                                      <th>Modifier</th>
                                      <th>Supprimer</th>
                                  	</tr>
                              </thead>
                              <tbody>
                              <?php 
                              	foreach($LesEvenements as $UnEvenement)
                              	{
                              		$id = $UnEvenement['idE'];
                              		$lieu = $UnEvenement['lieu'];
                              		$dateE = $UnEvenement['dateE'];
                              		$heureE = $UnEvenement['heureE'];
                              		$Description = $UnEvenement['Description'];
                              		$idAdmin = $UnEvenement['idAdmin'];

                              		echo "<tr>
			                                  <td>$lieu</td>
			                                  <td>$dateE</td>
			                                  <td>$heureE</td>
			                                  <td>$Description</td>
			                                  <td>$idAdmin</td>
			                                  <td><a href='index.php?uc=administrer&action=modifierEvenements&id=$id' >Modifier : $Description </a></td>
			                                  <td><a href='#' onClick=\"confirmeSupprimeEvenement('".$id."')\">Supprimer : $Description </a></td>
			                              </tr>";
                              	}
                               ?>
                          </tbody>
                      </table>
                  </div>
              </div>
        </div>
        </section>
        </aside>