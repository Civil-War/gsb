<aside class="right-side">
	<header class="panel-heading">
          Formulaire d'envoie de message
    </header>
    <div class="row"><br/>
    <div class="col-md-12">
	<div class="inner contact">
	                <!-- Form Area -->
	                <div class="contact-form">
	                    <!-- Form -->
	                    <form id="contact-us" method="POST" action="index.php?uc=administrer&action=envoyerMessage">
	                        <!-- Left Inputs -->
	                        <div class="col-xs-6 wow animated slideInLeft" data-wow-delay=".5s">
	                            <!-- Email -->
	                            <input type="email" name="adresse" id="mail" required="required" class="form" placeholder="Email du destinataire.." />
	                            <!-- Subject -->
	                            <input type="text" name="sujet" id="subject" required="required" class="form" placeholder="Sujet.." />
	                        </div><!-- End Left Inputs -->
	                        <!-- Right Inputs -->
	                        <div class="col-xs-6 wow animated slideInRight" data-wow-delay=".5s">
	                            <!-- Message -->
	                            <textarea name="message" id="message" class="form textarea"  placeholder="Message.."></textarea>
	                        </div><!-- End Right Inputs -->
	                        <!-- Bottom Submit -->
	                        <div class="relative fullwidth col-xs-12">
	                            <!-- Send Button -->
	                            <button type="submit" id="submit" name="submit" class="form-btn semibold">Envoyer</button><br/>
	                        </div><!-- End Bottom Submit -->
	                        <!-- Clear -->
	                        <div class="clear"></div>
	                    </form>
	                    <!-- Your Mail Message -->
	                    <div class="mail-message-area">
	                        <!-- Message -->
	                        <div class="alert gray-bg mail-message not-visible-message">
	                            <strong>Votre message a bien été envoyer !</strong> 
	                        </div>
	                    </div>

	                </div><!-- End Contact Form Area -->
	            </div><!-- End Inner -->
	        </div>
	    </div>

</aside>
