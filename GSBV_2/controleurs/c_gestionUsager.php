<?php
    if(!isset($_REQUEST['action']))
    {
        $action = 'accueilUsager';
    }
    else
    {
        $action = $_REQUEST['action'];
    }

    switch($action)
    {
        case 'accueilUsager':	
        {	
			$LesEvenements = $pdo->getEventPeople($_SESSION['idEmployer']);
            include ('vues/Usager/v_evenements.php');
            break;
        }

        case 'seDeconnecter':
        {
            unset($_SESSION['login']);
            echo "<script>document.location.replace('index.php');</script>";
        }
		
		// case 'ConsulterEvenement':
        // {
			// $LesEvenements = $pdo->getLesEvenementsUsager($_SESSION['login']);
            // include ('vues/Usager/v_evenements.php');
            // break;
        // }
		
		case 'ConsulterDetailsEvenement':
        {
			$idEvenement=$_REQUEST['id'];
			$LesDetails = $pdo->getLesDetailsUsager($idEvenement);
			$PremiereLettre = $pdo->VerifierPremiereLettre($idEvenement);
            include ('vues/Usager/v_details.php');
            break;
        }
		
		case 'supprimerEvenements':
		{
			$id = $_REQUEST['id'];
			$suppresion = $pdo->SupprEvent($id);
			if($suppresion)
			{
				echo"<script>document.location.replace('index.php?uc=usager&action=accueilUsager')</script>";
			}
		}
    }
?>