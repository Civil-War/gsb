<?php
    if(!isset($_REQUEST['action']))
    {
        $action = 'seConnecterEnUsager';
    }
    else
    {
        $action = $_REQUEST['action'];
    }

    switch($action)
    {
        case 'seConnecterEnUsager':
        {
            if(!isset($_SESSION['login']))
            {
                if(isset($_POST["submit"]))
                {
                    $login = $_POST['login'];
                    $pass = $_POST['pass'];
                
                    $resu = $pdo->connexionUsager($login, $pass);
                    if ($resu) 
                    {
                        unset($_SESSION['email']);
                        $_SESSION['login'] = $login;
                        echo "<script>document.location.replace('index.php?uc=usager&action=accueilUsager');</script>";
                    }
                    else
                    {
                        $msgErreurs[] = "Erreur de login";
                        include ("vues/v_erreurs.php");
                    }
                }
                include ('vues/v_connexion.php');
                break;
            }
            else
            {
                echo "<script>document.location.replace('index.php?uc=usager&action=accueilUsager');</script>";
                break;
            }
        }

        case 'seConnecterEnAdmin':
        {
            if(!isset($_SESSION['email']))
            {
                if(isset($_POST["submit"]))
                {
                    $email = $_POST['email'];
                    $pass = $_POST['pass'];
                
                    $resu = $pdo->connexionAdmin($email, $pass);
                    if ($resu) 
                    {
                        unset($_SESSION['login']);
                        $_SESSION['email'] = $email;
                        echo "<script>document.location.replace('index.php?uc=administrer&action=accueilAdmin');</script>";
                    }
                    else
                    {
                        $msgErreurs[] = "Erreur de login";
                        include ("vues/v_erreurs.php");
                    }
                }
                include ('vues/v_connexionAdmin.php');
                break;
            }
            else
            {
                echo "<script>document.location.replace('index.php?uc=administrer&action=accueilAdmin');</script>";
                break;
            }
        }
    }
?>