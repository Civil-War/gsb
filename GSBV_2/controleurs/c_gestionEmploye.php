<?php
	 if(!isset($_REQUEST['q']))
    {
        $q = 'voirEmployers';
    }
    else
    {
        $q = $_REQUEST['q'];
    }

    switch($q)
    {
        case 'voirEmployers':
        {
            $LesEmployes = $pdo->getLesEmployes();
            include ('vues/Admin/v_voirEmployes.php');
            break;
        }
	
        case 'ajouterEmployers':
        {
            if(isset($_POST['submit']))
            {
                if($_POST['nom'] == "" || $_POST['prenom'] == "" || $_POST['login'] == "" || $_POST['mdp'] == "")
                {
                        echo "<script>alert('Certains champs sont manquants');</script>";
                }
                else
                {
                        $nom = $_POST['nom'];
                        $prenom = $_POST['prenom'];
                        $login = $_POST['login'];
                        $mdp = $_POST['mdp'];

                        $insertion = $pdo->ajoutEmployes($nom,$prenom,$login,$mdp);

                        if($insertion)
                        {
                            echo "<script>document.location.replace('index.php?uc=administrer&action=consulterEmployers&q=voirEmployers');</script>";
                        }
                }
            }
            include ('vues/Admin/v_ajouterEmployes.php');
            break;
        }
	case 'modifierEmployers':
        {	
            if(isset($_POST['submit']))
            {
                    $id = $_REQUEST['id'];
                    if($_POST['nom'] == "" || $_POST['prenom'] == "" || $_POST['login'] == "" || $_POST['mdp'] == "")
                    {
                            echo "<script>alert('Certains champs sont manquants');</script>";
                    }
                    else
                    {
                            $nom = $_POST['nom'];
                            $prenom = $_POST['prenom'];
                            $login = $_POST['login'];
                            $mdp = $_POST['mdp'];

                            $update = $pdo->updateEmployes($id,$nom,$prenom,$login,$mdp);

                            if($update)
                            {
                                    echo "<script>document.location.replace('index.php?uc=administrer&action=consulterEmployers&q=voirEmployers');</script>";
                            }
                    }
            }
            include ('vues/Admin/v_modifierEmployes.php');
            break;
        }

        case 'supprimerEmployers':
        {
            $id = $_REQUEST['id'];
            $supression = $pdo->SupprEmployes($id);
            if($supression)
            {
                    echo "<script>document.location.replace('index.php?uc=administrer&action=consulterEmployers&q=voirEmployers');</script>";
            }
            break;
        }
    }
?>