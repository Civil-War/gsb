 <?php
    if(!isset($_REQUEST['q']))
    {
        $q = 'voirEvenements';
    }
    else
    {
        $q = $_REQUEST['q'];
    }

    switch($q)
    {
        case 'voirEvenements':
        {
            $LesEvenements = $pdo->getLesEvenements();
            include ('vues/Admin/v_voirEvenements.php');
            break;
        }

        case 'supprimerEvenements':
        {
            $id = $_REQUEST['id'];
            $supression = $pdo->SupprEvent($id);
            if($supression)
			{
				echo "<script>document.location.replace('index.php?uc=administrer&action=consulterEvenements&q=voirEvenements');</script>";
			}
            break;
        }
    }
?>