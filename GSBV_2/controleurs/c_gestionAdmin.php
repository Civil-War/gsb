<?php
    if(!isset($_REQUEST['action']))
    {
        $action = 'accueilAdmin';
    }
    else
    {
        $action = $_REQUEST['action'];
    }

    switch($action)
    {
        case 'accueilAdmin':
        {
            $LesEvenements = $pdo->getLesEvenements();
            $LesEmployes = $pdo->getLesEmployes();
            include ('vues/Admin/v_accueilAdmin.php');
            break;
        }

        case 'consulterEvenements':
        {
            include ('controleurs/c_gestionEvenement.php');
            break;
        }
		
		case 'consulterEmployers':
        {
            include ('controleurs/c_gestionEmploye.php');
            break;
        }

        case 'envoyerMessage':
        {
            if(isset($_POST['submit']))
            {
                if($_POST['adresse'] == "" && $_POST['sujet'] == "" && $_POST['message'] == "")
                {
                    echo "<script>alert('Certains champs sont vides');</script>";
                }
                else
                {
                    $adresseD = $_POST['adresse'];
                    $sujet = $_POST['sujet'];
                    $message = $_POST['message'];

                    $envoieMessage = $pdo->envoyerMessage($adresseD,$_SESSION['email'],$sujet,$message);

                    if($envoieMessage)
                    {
                        echo "<script>alert('Le message a bien été envoyer');</script>";
                        echo "<script>document.location.replace('index.php?uc=administrer&action=accueilAdmin');</script>";
                    }
                    else
                    {
                        echo "<script>alert('L'expediteur est incorrecte');</script>";
                    }
                }
            }
            include ('vues/Admin/v_envoyerMessage.php');
            break;
        }
        case 'seDeconnecter':
        {
            unset($_SESSION['email']);
            echo "<script>document.location.replace('index.php');</script>";
        }
    }
?>