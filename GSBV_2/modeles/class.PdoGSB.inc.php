<?php
/** 
 * Classe d'accès aux données. 
 
 * Utilise les services de la classe PDO
 * pour l'application lafleur
 * Les attributs sont tous statiques,
 * les 4 premiers pour la connexion
 * $monPdo de type PDO 
 * $monPdoGsb qui contiendra l'unique instance de la classe
 *
 * @package default
 * @author Patrice Grand
 * @version    1.0
 * @link       http://www.php.net/manual/fr/book.pdo.php
 */

class PdoGSB
{   		
      	private static $serveur='mysql:host=192.168.1.125';
      	private static $bdd='dbname=gsb_ppe2016';
      	private static $user='olivier' ;    		
      	private static $mdp='btssio' ;	
        private static $monPdo;
        private static $monPdoGSB = null;
/**
 * Constructeur privé, crée l'instance de PDO qui sera sollicitée
 * pour toutes les méthodes de la classe
 */				
	private function __construct()
	{
		PdoGSB::$monPdo = new PDO(PdoGSB::$serveur.';'.PdoGSB::$bdd, PdoGSB::$user, PdoGSB::$mdp);
		PdoGSB::$monPdo->query("SET CHARACTER SET utf8");
	}
	public function _destruct()
        {
		PdoGSB::$monPdo = null;
	}
/**
 * Fonction statique qui crée l'unique instance de la classe
 *
 * Appel : PdoGSB = PdoGSB::getPdoGSB();
 * @return l'unique objet de la classe PdoGSB
 */
 
 	public function envoyerMessage($adresseD,$adresseE,$sujet,$message)
	{
		$verif = "select count(*) as nbLogin from employes where login='".$adresseD."'";
		$query = PdoGSB::$monPdo->query($verif);
		$fetch = $query->fetch();
		$nombreAdresse = $fetch['nbLogin'];

		$verifAdmin = "select count(*) as nbAdmin from administrateur where mail='".$adresseD."'";
		$queryA = PdoGSB::$monPdo->query($verifAdmin);
		$fetchA = $queryA->fetch();
		$nombreAdresseA = $fetchA['nbAdmin'];

		if($nombreAdresse >= 1 || $nombreAdresseA >= 1)
		{
			$req = "insert into messagerie values ('','".$adresseD."','".$adresseE."','".$sujet."','".$message."')";
			$res = PdoGSB::$monPdo->query($req);
			if($res)
			{
				return true;
			}
		}
		else
		{
			return false;
		}

	}
	

	public function nombreMessage($adresse)
	{
		$sql = "SELECT COUNT(*) as nbMessage FROM messagerie WHERE adresseReceptionneur='".$adresse."'";
		$req = PdoGSB::$monPdo->query($sql);
		$LaLigne = $req->fetch();
		$_SESSION['nbMessage'] = $LaLigne['nbMessage'];
	}

	public function chargerLesMessages($adresse)
	{
		$sql = "SELECT * FROM messagerie WHERE adresseReceptionneur='".$adresse."'";
		$req = PdoGSB::$monPdo->query($sql);
		$LesLignes = $req->fetchAll();
		return $LesLignes;
	}
 
 
 
	public static function getPdoGSB()
	{
		if(PdoGSB::$monPdoGSB == null)
		{
			PdoGSB::$monPdoGSB= new PdoGSB();
		}
		return PdoGSB::$monPdoGSB;
	}
	public function verifierUser($l, $m){
		$req = "select count(*) as nb from employes where login='".$l."' and mdp='".$m."'";
		$res = PdoGSB::$monPdo->query($req);
		$LaLigne = $res->fetch();
		$nombre = $LaLigne['nb'];
		if ($nombre == 1){
			$_SESSION["login"] = $_POST["login"];
			return true;
		}
		else
		{
			return false;
		}
	}

	public function getLesEvenements()
	{
		$req = "select * from evenement";
		$res = PdoGSB::$monPdo->query($req);
		$LesLignes = $res->fetchAll();
		return $LesLignes;

	}

        public function ajoutEmployes($nom,$prenom,$login,$mdp)
        {
            $req = "insert into employes values ('','".$nom."','".$prenom."','".$login."','".$mdp."')";
            $res = PdoGSB::$monPdo->query($req);
            if($res)
            {
                return true;
            }
            return false;
        }
        
	/*public function getLesEvenementsUsager($login)
	{
		$req1 = "select idEm from employes where login='".$login."'";
		$res1 = PdoGSB::$monPdo->query($req1);
		$LeId = $res1->fetch();
		var_dump($LeId['idEm']);
		$req = "select * from evenement inner join convoquer on evenement.idE=convoquer.idE where convoquer.idEm = '".$LeId."'order by DateE desc";
		//$req = "select * from evenement where idEm='".$LeId['idEm']."' order by DateE desc";
		$res = PdoGSB::$monPdo->query($req);
		$lesLignes = $res->fetchAll();
		return $lesLignes;
	}*/
	
	public function getLesDetailsUsager($idEvenement)
	{

		$req = "select * from evenement where idE='".$idEvenement."'";
		$res = PdoGSB::$monPdo->query($req);
		$lesDetails = $res->fetchAll();
		return $lesDetails;
	}
        
	public function VerifierPremiereLettre($idEvenement)
	{
		$req="SELECT DISTINCT(LEFT(Description, 1)) FROM evenement where idE='".$idEvenement."'";
		$PremiereLettre = PdoGSB::$monPdo->query($req);
		
		
		switch ($PremiereLettre)
		{
		

    case "A":
	$L="l' ";	
        return $L;
        break;
    case "E":
	$L="l'";
        return $L;
        break;
    case "I":
	$L="l'";
        return $L;
		
        break;
	case "O":
	$L="l'";
        return $L;
        break;
    case "U":
	$L="l'";
        return $L;
        break;
    case "Y":
	$L="l'";
        return $L;
        break;
		 default:
	$L="la/le ";
       return $L;
		}
	}
	
	public function dateEvenement($idEvenement){

		$req = "select dateE from evenement where idE=".$idEvenement;
		$res = PdoGSB::$monPdo->query($req);
		$DateE = $res->fetch();
		
		$DateEtimestamp = strtotime($DateE[0]);
		$DateActuelletimestamp = time();

		
		
		
		$Delai=$DateEtimestamp-$DateActuelletimestamp;
		
		$delaiHeures = $Delai / 3600;
	
		return $delaiHeures;

	}
	
	
	public function ajouterEvenement($lieu, $date, $heure, $description,$idtype,$idEmploye, $details){
		$req = "insert into evenement (lieu,dateE,heureE,Description,idType,idEm,Details) values (".$lieu.", ".$date.",".$heure.",".$description;",".$idtype.",".$idEmploye.",".$details.")";
		$res = PdoGSB::$monPdo->query($req);
	}

	
	
	
	public function rechercher($b){

		$lesEmployes=array();
		$req = "select * from employes where nom like '".$b."%'";
		$res = PdoGSB::$monPdo->query($req);
		$unEmploye = $res->fetchAll();
		$lesEmployes[] = $unEmploye;
		return $lesEmployes;
	}

	public function getLesEmployes()
	{
		$req = "select * from employes";
		$res = PdoGSB::$monPdo->query($req);
		$LesLignes = $res->fetchAll();
		return $LesLignes;

	}

	public function getInfo($id){
		$lesInfos=array();
		$req = "select * from employes where idEm = ".$id;
		$res = PdoGSB::$monPdo->query($req);
		$uneInfo = $res->fetch();
		$lesInfos[] = $uneInfo;
		return $lesInfos;
	}

	
	
	
	public function getEventPeople($id)
	{
		$req = "select COUNT(*) as nbEvenement , evenement.* from evenement inner join convoquer on evenement.idE=convoquer.idE where convoquer.idEm = ".$id;
		$res = PdoGSB::$monPdo->query($req);
		$LesLignes = $res->fetchAll();
		
		foreach($LesLignes as $uneLigne)
		{
			$fraise = $uneLigne['nbEvenement'];
		}
		
		
		var_dump($fraise);
		//var_dump($LesLignes);
		//var_dump($LesLignes['nbEvenement']);
		$leResu = array();
		foreach($LesLignes as $unEvt){
			$difference = $this->dateEvenement($unEvt['idE']);
			$unEvt['diff'] = $difference;
			$leResu[] = $unEvt;
		}

		return $leResu;
	}
	
	
	/*
	public function getLesEvenementsUsager($login)
	{
		$req1 = "select idEm from employes where login='".$login."'";
		$res1 = PdoGSB::$monPdo->query($req1);
		$LeId = $res1->fetch();
		$req = "select * from evenement where idEm='".$LeId['idEm']."'order by DateE asc";
		$res = PdoGSB::$monPdo->query($req);
		$lesLignes = $res->fetchAll();
		$leResu = array();
		foreach($lesLignes as $unEvt){
			$difference = $this->dateEvenement($unEvt['idE']);
			$unEvt['diff'] = $difference;
			$leResu[] = $unEvt;
		}

		return $leResu;
	
	
	
	*/
	
	
	
	

	public function getServicePeople($id){
		$req = "select libelle from service inner join employes on employes.idS=service.idS  where employes.idEm = ".$id;
		$res = PdoGSB::$monPdo->query($req);
		$libel = $res->fetchAll();
		return $libel[0];
	}

	public function getIdEmploye($nom){
		$req = "select idEm from employes where nom = '".$nom."'";
		$res = PdoGSB::$monPdo->query($req);
		$resul = $res->fetch();
		return $resul[0];
	}

	public function Ajouterevent($id, $event){
		$req = "insert into participe values (".$id.", ".$event.")";
		$res = PdoGSB::$monPdo->query($req);
	}

	
	public function Deleteevent($id, $event){
		$req = "delete from participe where idE = ".$event." and idEm = ".$id;
		$res = PdoGSB::$monPdo->query($req);
	}

	public function getInfosEvent($info)
	{
		$lesInfos=array();
		$req = "select * from evenement where idE =".$info;
		//echo $req;
		$res = PdoGSB::$monPdo->query($req);
		$infoEvent = $res->fetchAll();
		$lesInfos[] = $infoEvent;
		//echo var_dump($lesInfos);
		return $lesInfos;
	}

	public function getEmployesEvent($idevent)
	{
		$lesEmployes=array();
		$req = "select nom from employes inner join convoquer on employes.idEm=convoquer.idEm where convoquer.idE = ".$idevent;
		//echo "<br/>".$req;
		$res = PdoGSB::$monPdo->query($req);
		$Employe = $res->fetchAll();
		$lesEmployes[] = $Employe;
		return $lesEmployes;
	}

	public function ValidModif($id, $inti, $lieu, $date, $heure)
	{
		$req = "UPDATE evenement SET lieu='".$lieu."', dateE='".$date."', heureE='".$heure."', Description='".$inti."' WHERE idE=".$id;
		$res = PdoGSB::$monPdo->query($req);
	}

	public function SupprEvent($id)
	{
		$req = "DELETE from evenement WHERE idE=".$id;
		$res = PdoGSB::$monPdo->query($req);
		if($res)
		{
			return true;
		}
		return false;
	}
	
	public function updateEmployes($id,$nom,$prenom,$login,$mdp)
	{
		$sql = "update employes set nom=$nom,prenom=$prenom,login=$login,mdp=$mdp where idEm=$id;";
		$req = PdoGSB::$monPdo->query($sql);
		if($req)
		{
			return true;
		}
		return false;
	}
	
	public function SupprEmployes($id)
	{
		$req = "DELETE from employes WHERE idEm=".$id;
		$res = PdoGSB::$monPdo->query($req);
		if($res)
		{
			return true;
		}
		return false;
	}

	public function AddEvent($inti, $lieu, $date, $heure)
	{
		$req = "select max(idE) from evenement";
		$res = PdoGSB::$monPdo->query($req);
		$id = $res->fetchAll();
		$id[0][0]++;
		$req = "INSERT INTO evenement VALUES (".$id[0][0].",'".$lieu."','".$date."','".$heure."','".$inti."')";
		echo $req;
		$res = PdoGSB::$monPdo->query($req);
	}

	public function ScriptMail()
	{
		ini_set("SMTP","80.12.242.10" );
		ini_set("sendmail_from","louis.aubertot@gmail.com" );
		$date = date("Y")."-".date("m")."-".(date("d")+2);
		$req = "select * from evenement where dateE < '".$date."'";
		$res = PdoGSB::$monPdo->query($req);
		$event = $res->fetchAll();
		$lesEvenements[] = $event;
		foreach ($lesEvenements[0] as $c) {
			$message = "N'oubliez pas la ".$c["Description"]." du : ".$c["dateE"].". Lieu : ".$c["lieu"].". Heure : ".$c["heureE"].". On compte sur vous !";
			$message = wordwrap($message, 70, "\r\n");
			$req = "select login from employes inner join participe on employes.idEm=participe.idEm where idE = ".$c['idE'];
			$res = PdoGSB::$monPdo->query($req);
			if($res){
				$mail = $res->fetchAll();
				$lesMails[] = $mail;
				foreach ($lesMails[0] as $m){
					$email = $m["login"];
					mail($email, 'Réunion', $message);
				}
			}
		}

	}

	public function connexionAdmin($email,$pass)
	{
		$req = "select count(*) as nbAdmin , id , nom , prenom from administrateur where mail ='".$email."' and pass='".$pass."'";
		$res = PdoGSB::$monPdo->query($req);
		$LaLigne = $res->fetch();
		$nombre = $LaLigne['nbAdmin'];
		if ($nombre == 1)
		{
			$_SESSION['idAdmin'] = $LaLigne['id'];
			$_SESSION['nomAdmin'] = $LaLigne['nom'];
			$_SESSION['prenomAdmin'] = $LaLigne['prenom'];
			return true;
		}
		return false;		
	}

	public function connexionUsager($login,$pass)
	{
		$req = "select count(*) as nb , idEm from employes where login ='".$login."' and mdp='".$pass."'";
		$res = PdoGSB::$monPdo->query($req);
		$LaLigne = $res->fetch();
		$nombre = $LaLigne['nb'];
		if ($nombre == 1)
		{
			$_SESSION['idEmployer'] = $LaLigne['idEm'];
			return true;
		}
		return false;		
	}
}


/*$test = PdoGSB::getPdoGSB();
echo "<pre>";
print_r($test->getEventPeople(1));
echo "</pre>";*/
?>